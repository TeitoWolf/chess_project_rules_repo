# README #

Load up that unnity package Dawg. Everything should work out of the box.

### What is this repository for? ###

This repo is used as a back-up for the code as well as to eventually merge code with
my other team members eventually.

### How do I get set up? ###

Everything should run smooth as silk when you drag the BoardManager Prefab and
Plane Prefab into the scene.  From there, when you run the game, make sure you have 
the Scene tab open in order to see the drawn grid as there is no physical board.

### Contribution guidelines ###

All code was done by Anthony "TeitoWolf" Hernandez

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact