﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {

    public static Board Instance { set; get; }
    private bool [,] moveIsAllowed { set; get; }

    public Piece[,] SpawnedPiece { set; get; }
    Piece selectedPiece;

    BoardStatus status;
    public int[] enPassant { set; get; }


    const float TileSize = 1.0f;
    const float TileOffset = .5f;

    int SelectedX = -1;
    int SelectedZ = -1;

    public List<GameObject> ChessArmyPrefabs;
    List<GameObject> ChessArmyOnBoard = new List<GameObject>();

    

    private void Start()
    {
        Instance = this;
        SpawnEntireWhiteArmy();
        status.WhiteTurn = true;
    }

    private void Update()
    {
        UpdateSelection();
        DrawChessBoard();

        if (Input.GetMouseButtonDown(0))
        {
            if (SelectedX >= 0 && SelectedZ >= 0)
            {
                if (selectedPiece == null) //Select the Piece
                {
                    SelectThePiece(SelectedX, SelectedZ);
                }
                else //Move the Piece
                {
                    MoveThePiece(SelectedX, SelectedZ);
                }
            }
        }
    }

    void SelectThePiece(int x, int z)
    {
        if (SpawnedPiece[x, z] == null)
            return;

        if (SpawnedPiece[x, z].isWhite != status.WhiteTurn)
            return;

        selectedPiece = SpawnedPiece[x, z];
        moveIsAllowed = selectedPiece.MoveIsPossible();
    }

    void MoveThePiece(int x, int z)
    {
        if (moveIsAllowed[x,z]){

            Piece c = SpawnedPiece[x, z];

            if(c != null && c.isWhite != status.WhiteTurn)
            {
                //Capture

                if (c.GetType() == typeof(WhiteKing))
                {                    
                    return;
                }
                else if(c.GetType() == typeof(BlackKing))
                {                  
                    return;
                }


                //En Passant
                if(x == enPassant[0] && z == enPassant[1])
                {
                    //White Passant
                    if (status.WhiteTurn)
                    {
                        c = SpawnedPiece[x, z - 1];
                    }
                    //Black Passant
                    else
                    {
                        c = SpawnedPiece[x, z - 1];
                    }
                }

                //Reset En Passant index
                enPassant[0] = -1;
                enPassant[1] = -1;
                if(selectedPiece.GetType() == typeof(WhitePawn))
                {
                    //Promotion (Only promotes to a Queen)
                    if(z == 7)
                    {
                        Destroy(selectedPiece.gameObject);
                        SpawnArmyUnit(4, x, z);
                        selectedPiece = SpawnedPiece[x, z];
                    }
                    if (selectedPiece.CurrentZ == 1 && z == 3)
                        enPassant[0] = x; enPassant[1] = z - 1;

                }
                if (selectedPiece.GetType() == typeof(BlackPawn))
                {
                    if (selectedPiece.CurrentZ == 6 && z == 4)
                        enPassant[0] = x; enPassant[1] = z + 1;

                }

            }


            SpawnedPiece[selectedPiece.CurrentX, selectedPiece.CurrentZ] = null;
            selectedPiece.transform.position = GetCenterTile(x, z);
            selectedPiece.SetPosition(x, z);
            SpawnedPiece[x, z] = selectedPiece;


        }

        selectedPiece = null;
    }

    ///<summary>
    /// Checks the mouse position and "highlights" the grid square it's in
    ///</summary>
    void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50f, 
            LayerMask.GetMask("ChessBoardLayer") ))
        {
            SelectedX = (int)hit.point.x;
            SelectedZ = (int)hit.point.z;
        }
        else
        {
            SelectedX = -1;
            SelectedZ = -1;
        }
    }

   ///<summary>
   /// Grabs and Instantiate Gameobjects in the ChessArmyPrefab
   ///</summary>
    void SpawnArmyUnit(int index, int x, int z)
    {
        GameObject go = Instantiate(ChessArmyPrefabs[index], GetCenterTile(x,z), 
            Quaternion.identity) as GameObject;

        go.transform.SetParent(transform);

        SpawnedPiece[x, z] = go.GetComponent<Piece>();
        SpawnedPiece[x, z].SetPosition(x, z);

        ChessArmyOnBoard.Add(go);

    }

    ///<summary>
    /// Creates a Visual Grid during Runtime in the Scene View
    ///</summary>
    void DrawChessBoard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for(int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);

            for (int j = 0; j <= 8; j++)
            {
                start = Vector3.right * i;
                Debug.DrawLine(start, start + heightLine);
            }
        }

        if(SelectedX >=0 && SelectedZ >= 0)
        {
            Debug.DrawLine(
                Vector3.forward * SelectedZ + Vector3.right * SelectedX,
                Vector3.forward * (SelectedZ + 1) + Vector3.right * (SelectedX + 1));
        }

    }

    ///<summary>
    /// Does whats on the tin. Grabs the center position based on the Chessboard Grid
    ///</summary>
    private Vector3 GetCenterTile(int x, int z)
    {
        Vector3 originPoint = Vector3.zero;
        originPoint.x += (TileSize * x) + (TileOffset);
        originPoint.z += (TileSize * z) + (TileOffset);
        return originPoint;
    }

    ///<summary>
    /// Spawns the White Army; Should be in Index 0 - 7
    ///</summary>
    void SpawnEntireWhiteArmy()
    {
        ChessArmyOnBoard = new List<GameObject>();
        SpawnedPiece = new Piece[8, 8];
        enPassant = new int[2] { -1, -1 };

        //Spawn the White Mans
        //King
        SpawnArmyUnit(5, 3, 0);

        //Queen
        SpawnArmyUnit(4, 4, 0);

        //Bishop
        SpawnArmyUnit(3, 5, 0);
        SpawnArmyUnit(3, 2, 0);

        //Knight
        SpawnArmyUnit(2, 6, 0);
        SpawnArmyUnit(2, 1, 0);

        //Rook
        SpawnArmyUnit(1, 7, 0);
        SpawnArmyUnit(1,0, 0);

        //Pawns
        for (int i = 0; i < 8; i++)
        {
            SpawnArmyUnit(0, i, 1);
        }
    }

}

///<summary>
///Checks the board status
///</summary>
public struct BoardStatus
{
    bool isWhiteTurn;
    int plies;
    int moves;

    ///<summary>
    /// True if White's Turn, false otherwise
    ///</summary>
    public bool WhiteTurn
    {
        get { return isWhiteTurn; }
        set { isWhiteTurn = value; }
    }

    ///<summary>
    /// True if Black's Turn, false otherwise
    ///</summary>
    public bool BlackTurn
    {
        get { return !isWhiteTurn; }
        set { isWhiteTurn = !value; }
    }

    ///<summary>
    /// Tracks the # of plies (Half turns; Black & White moving counts as 2 plies)
    ///</summary>
    public int Ply
    {
        get { return plies; }
        set { plies = value; }
    }

    ///<summary>
    /// Tracks the # of moves (Full turns; Black & white moving counts as 1 turn)
    ///</summary>
    public int Moves
    {
        get { return moves; }
        set { moves = value; }
    }
}
