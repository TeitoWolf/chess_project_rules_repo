﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteKnight : WhitePiece {

    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];

        //Forward
        KnightMove(CurrentX - 1, CurrentZ + 2, ref r);
        KnightMove(CurrentX + 1, CurrentZ + 2, ref r);

        //Back
        KnightMove(CurrentX - 1, CurrentZ - 2, ref r);
        KnightMove(CurrentX + 1, CurrentZ - 2, ref r);

        //Right
        KnightMove(CurrentX + 2, CurrentZ + 1, ref r);
        KnightMove(CurrentX + 2, CurrentZ - 1, ref r);

        //Left
        KnightMove(CurrentX - 2, CurrentZ + 1, ref r);
        KnightMove(CurrentX - 2, CurrentZ - 1, ref r);

        return r;
    }

    public void KnightMove(int x, int z, ref bool[,] r)
    {
        Piece c;

        if(x >= 0 && x < 8  && z >= 0 && z < 8)
        {
            c = Board.Instance.SpawnedPiece[x, z];
            if (c == null)
                r[x, z] = true;
            else if (isWhite != c.isWhite)
                r[x, z] = true;
        }
    }
}
