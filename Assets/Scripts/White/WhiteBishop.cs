﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBishop : WhitePiece {

    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];

        Piece c;
        int i, j;

        // Top Left
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i--;
            j++;
            if (i < 0 || j >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //TopRight
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i++;
            j++;
            if (i >= 8 || j >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //BottomLeft
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i--;
            j--;
            if (i < 0 || j < 0)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //Bottom Right
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i++;
            j--;
            if (i >= 8 || j < 0)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        return r;
    }
}

