﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteQueen : WhitePiece {

    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];

        Piece c;
        int i, j;


        //Forward Diagonal Left
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i--;
            j++;
            if (i < 0 || j >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //Forward Diagonal Right
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i++;
            j++;
            if (i >= 8 || j >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //Back Diagonal Left
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i--;
            j--;
            if (i < 0 || j < 0)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //Back Diagonal Right
        i = CurrentX;
        j = CurrentZ;

        while (true)
        {
            i++;
            j--;
            if (i >= 8 || j < 0)
                break;

            c = Board.Instance.SpawnedPiece[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;

                break;
            }
        }

        //Right
        i = CurrentX;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, CurrentZ];
            if (c == null)
                r[i, CurrentZ] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[i, CurrentZ] = true;

                break;
            }
        }

        //Left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i < 0)
                break;

            c = Board.Instance.SpawnedPiece[i, CurrentZ];
            if (c == null)
                r[i, CurrentZ] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[i, CurrentZ] = true;

                break;
            }
        }

        //Forward
        i = CurrentZ;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Board.Instance.SpawnedPiece[CurrentX, i];
            if (c == null)
                r[CurrentX, i] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[CurrentX, i] = true;

                break;
            }
        }

        //Back
        i = CurrentZ;
        while (true)
        {
            i--;
            if (i < 0)
                break;

            c = Board.Instance.SpawnedPiece[CurrentX, i];
            if (c == null)
                r[CurrentX, i] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[CurrentX, i] = true;

                break;
            }
        }

        return r;
    }
}
