﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteRook : WhitePiece {

    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];

        Piece c;
        int i;

        //Right
        i = CurrentX;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Board.Instance.SpawnedPiece[i, CurrentZ];
            if (c == null)
                r[i, CurrentZ] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[i, CurrentZ] = true;

                break;
            }
        }

            //Left
            i = CurrentX;
            while (true)
            {
                i--;
                if (i < 0)
                    break;

                c = Board.Instance.SpawnedPiece[i, CurrentZ];
                if (c == null)
                    r[i, CurrentZ] = true;
                else
                {
                    if (c.isWhite != isWhite)
                        r[i, CurrentZ] = true;

                    break;
                }
            }

            //UP
            i = CurrentZ;
            while (true)
            {
                i++;
                if (i >= 8)
                    break;

                c = Board.Instance.SpawnedPiece[CurrentX, i];
                if (c == null)
                    r[CurrentX, i] = true;
                else
                {
                    if (c.isWhite != isWhite)
                        r[CurrentX, i] = true;

                    break;
                }
            }

            //Down
            i = CurrentZ;
            while (true)
            {
                i--;
                if (i < 0)
                    break;

                c = Board.Instance.SpawnedPiece[CurrentX, i];
                if (c == null)
                    r[CurrentX, i] = true;
                else
                {
                    if (c.isWhite != isWhite)
                        r[CurrentX, i] = true;

                    break;
                }
            }
        return r;
    }
}
