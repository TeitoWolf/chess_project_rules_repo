﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteKing : WhitePiece
{

    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];

        MoveKing(CurrentX + 1, CurrentZ, ref r); //Forward
        MoveKing(CurrentX - 1, CurrentZ, ref r); //Back
        MoveKing(CurrentX, CurrentZ - 1, ref r); //Left
        MoveKing(CurrentX, CurrentZ + 1, ref r); //Right
        MoveKing(CurrentX + 1, CurrentZ + 1, ref r); //Forward Right
        MoveKing(CurrentX + 1, CurrentZ - 1, ref r); //Forward Left
        MoveKing(CurrentX - 1, CurrentZ - 1, ref r); //Back Left
        MoveKing(CurrentX - 1, CurrentZ + 1, ref r); //Back Right

        return r;
    }

    public void MoveKing(int x, int z, ref bool[,] r)
    {
        Piece c;

        if(x >= 0 && x < 8 && z >= 0 && z < 8)
        {
            c = Board.Instance.SpawnedPiece[x, z];

            if (c == null)
                r[x, z] = true;
            else if (isWhite != c.isWhite)
                r[x, z] = true;

        }
    }
}
