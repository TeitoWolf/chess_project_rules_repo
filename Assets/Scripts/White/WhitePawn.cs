﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhitePawn : WhitePiece {

    


    public override bool[,] MoveIsPossible()
    {
        bool[,] r = new bool[8, 8];
        Piece c, c2;

        if (isWhite)
        {
            //Diagonal Left
            if (CurrentX != 0 && CurrentZ != 7)
            {
                //En Passant is legal
                int[] p = Board.Instance.enPassant;
                if (p[0] == CurrentX - 1 && p[1] == CurrentZ + 1)
                    r[CurrentX - 1, CurrentZ + 1] = true;

                c = Board.Instance.SpawnedPiece[CurrentX - 1, CurrentZ + 1];
                if (c != null && !c.isWhite)
                    r[CurrentX - 1, CurrentZ + 1] = true;

                
                    
            }

            //Diagonal Right
            if (CurrentX != 7 && CurrentZ != 0)
            {
                //En Passant is Legal
                int[] p = Board.Instance.enPassant;
                if (p[0] == CurrentX + 1 && p[1] == CurrentZ + 1)
                    r[CurrentX + 1, CurrentZ + 1] = true;

                c = Board.Instance.SpawnedPiece[CurrentX + 1, CurrentZ + 1];
                if (c != null && !c.isWhite)
                    r[CurrentX - 1, CurrentZ + 1] = true;

            }

            //Foward
            if(CurrentZ != 7)
            {
                c = Board.Instance.SpawnedPiece[CurrentX, CurrentZ + 1];
                if (c == null)
                    r[CurrentX, CurrentZ + 1] = true;
            }

            //First-Turn Charge
            if (CurrentZ == 1)
            {
                c = Board.Instance.SpawnedPiece[CurrentX, CurrentZ + 1];
                c2 = Board.Instance.SpawnedPiece[CurrentX, CurrentZ + 2];
                if (c == null & c2 == null)
                    r[CurrentX, CurrentZ + 2] = true;
            }
        }
        return r;
    }

}
