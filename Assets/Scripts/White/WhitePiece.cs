﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WhitePiece : Piece {

    private void Awake()
    {
        isWhite = true;
    }

}
